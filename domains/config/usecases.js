const Sequelize = require('sequelize')
const { db } = require('../../db/sequelize')

const Config = db.define('config', {
  key: Sequelize.STRING,
  value: Sequelize.STRING,
})

const getIof = async () => {
  const iofValue = await Config.findAll({ where: { key: 'iof' } })

  return iofValue[0].dataValues
}

module.exports = {
  Config,
  getIof,
}

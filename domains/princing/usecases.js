const configUsecases = require('../config/usecases')

const enumHelper = {
  IS_IOF_DEFAULT: true,
  PRICING: {
    TAX_TYPE: {
      PERCENTUAL: 'P',
      FIXED: 'F',
      READJUSTED: 'B',
    },
    DISCOUNT_TYPE: {
      FIXED: 'F',
      PERCENTUAL: 'P',
    },
  },
}

const AMOUNT_LIMIT = 30000000000000

const shouldApplyIOF = ({ iof }) => {
  const iofIsNotForceBlocked = iof === undefined || iof !== false
  const iofIsMandatory = iof !== undefined && iof !== false

  return ((enumHelper.IS_IOF_DEFAULT && iofIsNotForceBlocked) || iofIsMandatory)
}

const getAmount = async () => 100

const getDescriptionByDirectAmount = async ({
  currency,
  amount,
  spread,
  exchangeRate,
  taxes,
  discounts,
  iof,
}) => ({
  currency,
  amount,
  spread,
  exchangeRate,
  taxes,
  discounts,
  iof,
})

const getDescriptionByBRLAmount = async ({
  totalBrlAmount,
  currency,
  spread,
  exchangeRate,
  taxes = [],
  discounts = [],
  iof,
}) => {
  const iofObj = await configUsecases.getIof()

  const taxesModified = shouldApplyIOF({ iof })
    ? taxes.concat([
      {
        name: 'IOF',
        type: enumHelper.PRICING.TAX_TYPE.PERCENTUAL,
        value: iofObj.value,
      }
    ])
    : taxes

  if (totalBrlAmount > AMOUNT_LIMIT) {
    throw new Error('invalidAmount')
  }

  const amount = await getAmount({
    totalBrlAmount,
    currency,
    spread,
    exchangeRate,
    taxes: taxesModified,
    discounts,
    iof,
  });


  const descriptionByDirectAmount = await getDescriptionByDirectAmount({
    currency,
    amount,
    spread,
    exchangeRate,
    taxes,
    discounts,
    iof,
  })

  descriptionByDirectAmount.totalBrlAmount = totalBrlAmount

  return descriptionByDirectAmount
}

// getDescriptionByBRLAmount({
//   totalBrlAmount: 111,
//   currency: 'BRL',
//   spread: 0.3,
//   exchangeRate: 3.2,
//   taxes: [],
//   discounts: [],
//   iof: null,
// })
// .then(console.log)

module.exports = {
  getDescriptionByBRLAmount,
}

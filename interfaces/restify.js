const restify = require('restify')
const Joi = require('joi')
const pricingUsecases = require('../domains/princing/usecases')

const server = restify.createServer({
  name: 'Pricing API',
  version: '1.0.0',
})

server.pre(restify.pre.sanitizePath())

const BrlAmountSchema = Joi.object().keys({
  totalBrlAmount: Joi.number().required(),
  currency: Joi.string().alphanum().min(3).max(3)
    .required(),
  spread: Joi.number().min(0).max(1).required(),
  exchangeRate: Joi.number().required(),
  iof: Joi.number(),
})

server.get('/v1/brl-amount/:receiveCurrency/:totalBrlAmount', async (req, res, next) => {
  const amount = {
    currency: req.params.receiveCurrency,
    totalBrlAmount: req.params.totalBrlAmount,
    spread: 0.1,
    exchangeRate: 3.3,
  }
  const amountObj = BrlAmountSchema.validate(amount);
  const response = await pricingUsecases.getDescriptionByBRLAmount(amountObj.value)

  res.json(response)
  return next()
})

module.exports = server

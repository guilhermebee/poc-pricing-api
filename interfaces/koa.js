const Koa = require('koa')
const Joi = require('joi')
const Router = require('koa-router')
const pricingUsecases = require('../domains/princing/usecases')

const router = new Router()
const app = new Koa()

const BrlAmountSchema = Joi.object().keys({
  totalBrlAmount: Joi.number().required(),
  currency: Joi.string().alphanum().min(3).max(3)
    .required(),
  spread: Joi.number().min(0).max(1).required(),
  exchangeRate: Joi.number().required(),
  iof: Joi.number(),
})

router.get('/v1/brl-amount/:receiveCurrency/:totalBrlAmount', async (ctx, next) => {
  const amount = {
    currency: ctx.params.receiveCurrency,
    totalBrlAmount: ctx.params.totalBrlAmount,
    spread: 0.1,
    exchangeRate: 3.3,
  }
  const amountObj = BrlAmountSchema.validate(amount);
  const response = await pricingUsecases.getDescriptionByBRLAmount(amountObj.value)

  ctx.body = response
});

app.use(router.routes())

module.exports = app

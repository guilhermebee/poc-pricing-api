const restify = require('./interfaces/restify')
const koa = require('./interfaces/koa')

restify.listen(8080, () => {
  console.log('restify - http://localhost:8080/v1/brl-amount/USD/100000')
})

koa.listen(3000, () => {
  console.log('koa - http://localhost:3000/v1/brl-amount/USD/100000')
})

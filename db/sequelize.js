const Sequelize = require('sequelize');

const db = new Sequelize('null', 'null', 'null', {
  dialect: 'sqlite',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  storage: `${__dirname}/db.sqlite`,
  operatorsAliases: false,
});

module.exports = {
  db,
}

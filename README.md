Na pasta "db" tem a conexão com o banco de dados Sqlite3, estou usando Sequelize para definir os models e usando o ORM.

Na pasta "interfaces" estou usando koa e restify, duas formas diferentes de cuidar do IO de dados da aplicação e demonstrar o desacoplamento do core business.

Em "migrations" estou demonstrando uma ideia inicial que seria utilizada junto com db-migrate, poderia pensar em colocar essa pasta dentro de "db" também.

Em "domains", defino os bounded context da aplicação seguindo o DDD, neste caso dividi em Config e Pricing para demonstração e criei um caso de uso da conversão de USD para BRL.

Estou usando Joi para fazer a validação dos dados na camada de IO (interfaces). Nesta camada também é usado o "model-presenter".

## Rodando

```
$ git clone
$ npm i
$ npm start

restify - http://localhost:8080/v1/brl-amount/USD/100000
koa - http://localhost:3000/v1/brl-amount/USD/100000
```
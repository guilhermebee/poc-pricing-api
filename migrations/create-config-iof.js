const { db } = require('../db/sequelize')
const { Config } = require('../domains/config/usecases')

db.sync()
  .then(() => Config.create({
    key: 'iof',
    value: '1.1',
  }))
  .then(res => console.log(res.toJSON()));
